# 基于docker和docker-compose部署带有登录认证的ELK日志收集环境

## 准备环境
* 系统版本: Ubuntu 22.04.x
* docker版本: Version 20.10.19
* docker-compose版本: Version 1.28.6
* ELK组件版本: Version 7.17.7

# Kibana界面：
![image-20221126175704153](images/image-20221126175704153.png)

# 基于prometheus监控elasticsearch
https://github.com/vvanholl/elasticsearch-prometheus-exporter

```go
容器运行ES:
/data/es/elk-docker-compose# docker exec -it elasticsearch /usr/share/elasticsearch/bin/elasticsearch-plugin install -b https://github.com/vvanholl/elasticsearch-prometheus-
exporter/releases/download/7.17.7.0/prometheus-exporter-7.17.7.0.zip
/data/es/elk-docker-compose# docker restart elasticsearch
验证指标数据:
http://<your-elasticsearch-host>:9200/_prometheus/metrics


二进制运行ES:
#/apps/elasticsearch/bin/elasticsearch-plugin install -b https://github.com/vvanholl/elasticsearch-prometheus-exporter/releases/download/7.17.7.0/prometheus-exporter-7.17.7.
0.zip
#systemctl  restart elasticsearch

验证指标数据:
http://<your-elasticsearch-host>:9200/_prometheus/metrics
```

![image-20221128231044657](images/image-20221128231044657.png)
